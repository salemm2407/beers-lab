const express = require("express");
const app = express();
const path = require("path");
const PunkAPIWrapper = require("punkapi-javascript-wrapper");
const punkAPI = new PunkAPIWrapper();

app.set("view engine", "pug");
app.set("views", __dirname + "/views");
app.use(express.static(path.join(__dirname, "public")));

app.get("/", (req, res, next) => {
  let data = {
    nav: ["Home", "Beers", "Random Beer"],
    footerTitle: "Develop by Ironhack ;)"
  };
  res.render("main", data);
}); // end of home

app.get("/beers", (req, res) => {
  punkAPI
    .getBeers()
    .then(beers => {
      res.render("beers", { beers });
    })
    .catch(error => {
      console.log(error);
    });
}); //end of get beers

app.get("/random", (req, res) => {
  punkAPI
    .getRandom()
    .then(beer => {
      console.log(`just one beer ${beer}`);
      res.render("random", {beer});
    })
    .catch(error => {
      console.log(error);
    });
}); // end of get random beer

app.listen(3000, () => {
  console.log(" server is up dude!! 🔥 🔥 🚀");
});
